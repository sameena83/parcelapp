import React from 'react'
import logo from '../../assets/images/image.png';
function Header() {
    return (
        <div className="main">
            <img className="img" src={logo} alt="image" ></img>
            <h1 className="header1">Welcome to My Parcel App</h1>
        </div>
    )
}

export default Header
