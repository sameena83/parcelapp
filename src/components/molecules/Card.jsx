import React from 'react'

import { FiPackage } from "react-icons/fi";
import { MdLocationOn } from "react-icons/md";
import { BsBuilding } from "react-icons/bs";
import { SiStatuspage } from "react-icons/si";
import { CgNotes } from "react-icons/cg";
function Card( {data} ) {
    const {
        eta,
        id,
        last_updated,
        location_coordinate_latitude,
        location_coordinate_longitude,
        location_id,
        location_name,
        location_status_ok,
        notes,
        parcel_id,
        sender,
        status,
        user_name,
        user_phone,
        verification_required,
    }=data;

    return (
            
             <article > 
            
            <div className = 'inline'>
                <p><FiPackage size='2em' color='BLUE' /> Packege-ID: {parcel_id} </p>
                <p><MdLocationOn size='2em' color='GREY'/> Delivery place: {location_name} </p>
                <p><BsBuilding size='2em' color='GREY' /> From: {sender} </p>
                <p><SiStatuspage size='2em' color='GREY'/> Status: {status} </p>
                <p><CgNotes size='2em' color='GREY'/> Details: {notes} </p>
                <hr />
            </div>
                
            </article>
             

        
    )
}

export default Card
